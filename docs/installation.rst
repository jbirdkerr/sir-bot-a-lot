.. highlight:: shell

============
Installation
============


Stable release
--------------

To install sir-bot-a-lot, run this command in your terminal:

.. code-block:: console

    $ pip install sir-bot-a-lot

This is the preferred method to install sir-bot-a-lot, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for sir-bot-a-lot can be downloaded from the `gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/mikefromit/sir-bot-a-lot

Or download the `tarball`_:

.. code-block:: console

    $ curl  -OL https://gitlab.com/mikefromit/sir-bot-a-lot/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _gitlab repo: https://gitlab.com/mikefromit/sir-bot-a-lot
.. _tarball: https://gitlab.com/mikefromit/sir-bot-a-lot/tarball/master
